# Battle Chess

A twist on the classic game of chess that adds an extra dimension of strategy through Final Fantasy/Pokemon battle mechanics.

Current Unity build in-use: **Unity 2018.1.5f1**

Current Unity Hub version in-use: **Unity Hub 0.18.0**