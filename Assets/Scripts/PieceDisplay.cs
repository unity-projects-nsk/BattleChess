﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PieceDisplay : MonoBehaviour {

    public Piece piece;

    public TextMeshProUGUI characterName;

    public GameObject characterSprite;
    
    public bool isWhite;
    public Piece.PieceType type;


    [Header("Hover Over Character Info")]
    public GameObject hoverCanvas;
    public GameObject background;
    public TextMeshProUGUI description;
    public TextMeshProUGUI level;
    public TextMeshProUGUI power;
    public TextMeshProUGUI defense;

    void Start () {
        characterName.text = piece.characterName;

        characterSprite.GetComponent<SpriteRenderer>().sprite = piece.characterSprite;
        //characterSprite.AddComponent<PolygonCollider2D>();

        background.GetComponent<Image>().sprite = piece.background;

        isWhite = piece.isWhite;
        type = piece.pieceType;

        description.text = piece.description;
        level.text = "Lvl. " + piece.level.ToString();
        power.text = "Pow. " + piece.power.ToString();
        defense.text = "Def. " + piece.defense.ToString();


	}
    
}
