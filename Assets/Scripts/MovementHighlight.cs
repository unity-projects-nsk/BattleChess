﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementHighlight : MonoBehaviour {

    public Piece.PieceType type;
    public bool isWhite;

	// Use this for initialization
	void Update() {
        type = GetComponentInChildren<PieceDisplay>().type;
        isWhite = GetComponentInChildren<PieceDisplay>().isWhite;
	}




}
