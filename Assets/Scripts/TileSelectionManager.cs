﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSelectionManager : MonoBehaviour {

    Camera cam;
    RaycastHit hit;
    Ray ray;

    string objectHit = null;
    Transform tileHit = null;

    void Start()
    {
        cam = Camera.main;
    }

    void Update()
    {
        ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, LayerMask.GetMask("Board")))
        {
            if (hit.transform != tileHit)
            {
                if (Input.GetMouseButtonDown(0))
                {

                }

                tileHit = hit.transform;
                objectHit = hit.transform.name;
                Debug.Log(objectHit);
            }
        }
    }
}
