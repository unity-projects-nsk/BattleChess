﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleInfo : MonoBehaviour {

    Animator showCaseAnim;
    GameObject popUp;
    TacticalViewController tacView;

    private void Start()
    {
        //popUp = this.GetComponentInParent<PieceDisplay>().hoverCanvas;
        popUp = gameObject.GetComponent<PieceDisplay>().hoverCanvas;

        showCaseAnim = Camera.main.GetComponent<Animator>();
        tacView = Camera.main.GetComponent<TacticalViewController>();
    }

    private void OnMouseOver()
    {
        if(!showCaseAnim.GetCurrentAnimatorClipInfo(0)[0].clip.name.Contains("boardShowCase") && !tacView.isPaused)
            popUp.SetActive(true);
            
    }

    private void OnMouseExit()
    {
            popUp.SetActive(false);
    }
}