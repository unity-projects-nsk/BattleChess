﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileHighlighter : MonoBehaviour {

    public Material highlightMaterial;

    Material originalMaterial;

    private void OnMouseEnter()
    {
        // Highlight w/ Color
        originalMaterial = gameObject.GetComponentInChildren<Renderer>().material;
        HighlightTile(gameObject);
        //gameObject.GetComponentInChildren<Renderer>().material = highlightMaterial;
    }

    private void OnMouseExit()
    {
        // Un-Highlight w/ Color
        UnHighlightTile(gameObject);
        //gameObject.GetComponentInChildren<Renderer>().material = originalMaterial;
    }

    public void HighlightTile(GameObject tileToHighlight)
    {
        tileToHighlight.GetComponentInChildren<Renderer>().material = highlightMaterial;
    }

    public void UnHighlightTile(GameObject tileToUnHighlight)
    {
        tileToUnHighlight.GetComponentInChildren<Renderer>().material = originalMaterial;
    }
}
