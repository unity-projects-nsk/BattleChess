﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PieceManager : MonoBehaviour {

    GameObject piece;
    Vector3 placeToSpawn;

    string objectName;
    bool isWhite;

    public Transform whiteParent, blackParent;
    public GameObject defaultPiece;

    private void Start()
    {
        placeToSpawn = GetOriginOfTile();

        if (defaultPiece != null)
        {
            piece = defaultPiece;
        }
        else
        {
            piece = null;
        }

        //SpawnPiece(piece);
    }

    public void SpawnPiece(GameObject pieceToSpawn)
    {
        if (pieceToSpawn != null && pieceToSpawn.GetComponentInChildren<PieceDisplay>().isWhite)
        {
            Instantiate(pieceToSpawn, placeToSpawn, Quaternion.identity, whiteParent);
        }
        if (pieceToSpawn != null && !pieceToSpawn.GetComponentInChildren<PieceDisplay>().isWhite)
        {
            Instantiate(pieceToSpawn, placeToSpawn, Quaternion.identity, blackParent);
        }
    }

    public Vector3 GetOriginOfTile()
    {
        objectName = gameObject.transform.name;
        Vector3 placeToSpawn = Vector3.zero;

        for (float i = 1; i < 9; i++)
        {
            if (objectName.Contains("A" + i))
                placeToSpawn = new Vector3(2 * i - 1, 0f, 1f);
            if (objectName.Contains("B" + i))
                placeToSpawn = new Vector3(2 * i - 1, 0f, 3f);
            if (objectName.Contains("C" + i))
                placeToSpawn = new Vector3(2 * i - 1, 0f, 5f);
            if (objectName.Contains("D" + i))
                placeToSpawn = new Vector3(2 * i - 1, 0f, 7f);
            if (objectName.Contains("E" + i))
                placeToSpawn = new Vector3(2 * i - 1, 0f, 9f);
            if (objectName.Contains("F" + i))
                placeToSpawn = new Vector3(2 * i - 1, 0f, 11f);
            if (objectName.Contains("G" + i))
                placeToSpawn = new Vector3(2 * i - 1, 0f, 13f);
            if (objectName.Contains("H" + i))
                placeToSpawn = new Vector3(2 * i - 1, 0f, 15f);
        }
        return placeToSpawn;
    }

}
