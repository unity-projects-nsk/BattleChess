﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TacticalViewController : MonoBehaviour {
    
    Animator anim;
    public GameObject mainMenuCanvas;
    public GameObject chessGameUI;
    public GameObject pauseMenuCanvas;
    public bool showCase = true;
    public bool isPaused = false;
    
    void Start () {
        anim = gameObject.GetComponent<Animator>();
	}
	
	void Update () {
        
        if((Input.GetKeyDown(KeyCode.LeftAlt) || Input.GetKeyDown(KeyCode.RightAlt)) && Input.GetKeyDown(KeyCode.F4))
        {
            QuitGame();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            anim.SetBool("isTactical", true);
        }
        else if (Input.GetKeyUp(KeyCode.E))
        {
            anim.SetBool("isTactical", false);
            anim.StopPlayback();
        }

        if(showCase)
            anim.SetBool("makeShowcase", true);
        else if(!showCase)
            anim.SetBool("makeShowcase", false);
        
        if (!anim.GetCurrentAnimatorClipInfo(0)[0].clip.name.Contains("boardShowCase"))
        {
            anim.speed = 1.0f;
        }

        if (Input.GetKeyDown(KeyCode.Escape) && !mainMenuCanvas.activeSelf)
            PauseGame();

        if (isPaused)
        {
            Time.timeScale = 0.0f;
        } else if (!isPaused)
        {
            Time.timeScale = 1.0f;
        }
    }

    public void StartGame()
    {
        showCase = false;
        anim.speed = 8f;
        mainMenuCanvas.SetActive(false);
        chessGameUI.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void PauseGame()
    {
        isPaused = !isPaused;
        pauseMenuCanvas.SetActive(!pauseMenuCanvas.activeSelf);
    }
}
