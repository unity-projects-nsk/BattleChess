﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Character", menuName = "Character")]
public class Piece : ScriptableObject {

    public string characterName;
    public Sprite characterSprite;
    //public Animator animationController; (use this later for character animation. Use static art for now)

    [Header("Hover Over Character Info")]
    // public string name; (use the variable above)
    public Sprite background;
    public PieceType pieceType;
    public bool isWhite;
    [TextArea]
    public string description;
    public int level;
    public int power;
    public int defense;
    
    public enum PieceType
    {
        King, Queen, Bishop, Knight, Rook, Pawn
    };
}
