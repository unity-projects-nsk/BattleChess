﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BoardManager : MonoBehaviour {

    public static BoardManager Instance { set; get; }
    private bool[,] allowedMoves { set; get; }

    public ChessPiece[,] ChessPieces { set; get; }
    private ChessPiece selectedPiece;

    private const float TILE_SIZE = 1.0f;
    private const float TILE_OFFSET = 0.5f;

    private int selectionX = -1;
    private int selectionY = -1;

    public List<GameObject> chessPiecePrefabs;
    private List<GameObject> activePieces;

    public bool isWhiteTurn = true;

    public TextMeshProUGUI turnText;

    private void Start()
    {
        Instance = this;
        SpawnAllChessPieces();
    }

    private void Update()
    {
        UpdateSelection();
        DrawChessboard();

        if(Input.GetMouseButtonDown(0))
        {
            if (selectionX >= 0 && selectionY >= 0)
            {
                if(selectedPiece == null)
                {
                    // Select the piece
                    SelectChessPiece(selectionX, selectionY);
                }
                else
                {
                    // Move the piece
                    MoveChessPiece(selectionX, selectionY);
                }
            }
        }

        if (isWhiteTurn)
            turnText.text = "White's Turn!";
        else if (!isWhiteTurn)
            turnText.text = "Black's Turn!";
    }

    private void SelectChessPiece(int x, int y)
    {
        if (ChessPieces[x, y] == null)
            return;

        if (ChessPieces[x, y].isWhite != isWhiteTurn)
            return;

        bool hasAtLeastOneMove = false;
        allowedMoves = ChessPieces[x, y].PossibleMove();
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++)
                if (allowedMoves[i, j])
                    hasAtLeastOneMove = true;

        selectedPiece = ChessPieces[x, y];
        BoardHighlighter.Instance.HighlightAllowedMoves(allowedMoves);
    }

    private void MoveChessPiece(int x, int y)
    {
        if (allowedMoves[x,y])
        {
            ChessPiece c = ChessPieces[x, y];

            if(c != null && c.isWhite != isWhiteTurn)
            {
                //Capture a piece ---- REPLACE THIS WITH BATTLE SYSTEM ----
                activePieces.Remove(c.gameObject);
                Destroy(c.gameObject);

                // If King dies
                if(c.GetType() == typeof(King))
                {
                    EndGame();
                }
            }

            ChessPieces[selectedPiece.CurrentX, selectedPiece.CurrentY] = null;
            selectedPiece.transform.position = GetTileCenter(x, y);
            selectedPiece.SetPosition(x, y);
            ChessPieces[x, y] = selectedPiece;
            isWhiteTurn = !isWhiteTurn;
        }

        BoardHighlighter.Instance.HideHighlights();
        selectedPiece = null;
    }

    private void UpdateSelection()
    {
        if (!Camera.main)
            return;

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 500.0f, LayerMask.GetMask("Board")))
        {
            selectionX = (int)hit.point.x;
            selectionY = (int)hit.point.z;
        }
        else
        {
            selectionX = -1;
            selectionY = -1;
        }
    }

    private void SpawnChessPiece(int index, int x, int y)
    {
        GameObject go = Instantiate(chessPiecePrefabs[index], GetTileCenter(x,y), Quaternion.Euler(0f,90f,0f)) as GameObject;
        go.transform.SetParent(transform);
        ChessPieces[x, y] = go.GetComponent<ChessPiece>();
        ChessPieces[x, y].SetPosition(x, y);
        activePieces.Add(go);
        //go.GetComponent<PieceDisplay>().characterSprite.AddComponent<PolygonCollider2D>();
    }

    private void SpawnAllChessPieces()
    {
        activePieces = new List<GameObject>();
        ChessPieces = new ChessPiece[8,8];

        // White Team
        // King
        SpawnChessPiece(0, 3, 0);
        // Queen
        SpawnChessPiece(1, 4, 0);
        // Rook
        SpawnChessPiece(2, 0, 0);
        SpawnChessPiece(2, 7, 0);
        // Bishop
        SpawnChessPiece(3, 2, 0);
        SpawnChessPiece(3, 5, 0);
        // Knight
        SpawnChessPiece(4, 1, 0);
        SpawnChessPiece(4, 6, 0);
        // Pawns
        for (int i = 0; i < 8; i++)
            SpawnChessPiece(5, i, 1);

        // Black Team
        // King
        SpawnChessPiece(6, 4, 7);
        // Queen
        SpawnChessPiece(7, 3, 7);
        // Rook
        SpawnChessPiece(8, 0, 7);
        SpawnChessPiece(8, 7, 7);
        // Bishop
        SpawnChessPiece(9, 2, 7);
        SpawnChessPiece(9, 5, 7);
        // Knight
        SpawnChessPiece(10, 1, 7);
        SpawnChessPiece(10, 6, 7);
        // Pawns
        for (int i = 0; i < 8; i++)
            SpawnChessPiece(11, i, 6);

    }

    private Vector3 GetTileCenter(int x, int y)
    {
        Vector3 origin = Vector3.zero;
        origin.x += (TILE_SIZE * x) + TILE_OFFSET;
        origin.z += (TILE_SIZE * y) + TILE_OFFSET;
        return origin;
    }

    private void DrawChessboard()
    {
        Vector3 widthLine = Vector3.right * 8;
        Vector3 heigthLine = Vector3.forward * 8;

        for (int i = 0; i <= 8; i++)
        {
            Vector3 start = Vector3.forward * i;
            Debug.DrawLine(start, start + widthLine);
            for (int j = 0; j <= 8; j++)
            {
                start = Vector3.right * j;
                Debug.DrawLine(start, start + heigthLine);
            }
        }

        if (selectionX >= 0 && selectionY >= 0)
        {
            Debug.DrawLine(
                Vector3.forward * selectionY + Vector3.right * selectionX,
                Vector3.forward * (selectionY + 1) + Vector3.right * (selectionX + 1));

            Debug.DrawLine(
                Vector3.forward * (selectionY + 1) + Vector3.right * selectionX,
                Vector3.forward * selectionY + Vector3.right * (selectionX + 1));
        }
    }

    private void EndGame()
    {
        if (isWhiteTurn)
            Debug.Log("White Team Wins!");
        else
            Debug.Log("Black Team Wins!");

        foreach (GameObject go in activePieces)
            Destroy(go);

        isWhiteTurn = true;
        BoardHighlighter.Instance.HideHighlights();
        SpawnAllChessPieces();
    }

}
